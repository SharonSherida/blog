---
title: Eerste Blog OP3
date: 1998-07-15
---

# Hoe (on)gezond lunchen studenten op Museumpark?

Voedingswetenschapper en docent-onderzoeker Canan Ziylan neemt jullie lunchgewoonten onder de loep. Hoe gezond lunchen de studenten van de Hogeschool Rotterdam? Vandaag: locatie Museumpark. 

Het grote HR (on)geluksonderzoek
De gemiddelde HR student is gelukkig en gezond. Toch vindt 7,1 procent van de studenten zichzelf depressief. 55 studenten die mee deden met het onderzoek en de vragenlijst invulden, hebben geprobeerd zelfmoord te plegen. 

##Uit het onderzoek is gebleken dat studenten gelukkig worden als ze op vakantie gaan (92,6%), goed contact hebben met hun vrienden (89,6%) en contact hebben met hun familie (70,4%). 

###Al deze gelukkige studenten hebben ook donkere dagen. De gemiddelde student heeft 7 donkere dagen per maand. Verdriet en stress maken het leven van de student lastig 

